#!/usr/bin/env bash
gem install bundler
bundle config --local github.https true
bundle config set --local path .bundle/gems && bundle
bundle exec asciidoctor-pdf -a allow-uri-read gitlab-runner-k3d-guide.adoc
bundle exec asciidoctor gitlab-runner-k3d-guide.adoc
