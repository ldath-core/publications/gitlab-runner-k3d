#!/usr/bin/env bash
mkdir public
cp -R images public/
cp gitlab-runner-k3d-guide.html public/index.html
cp gitlab-runner-k3d-guide.pdf public/
